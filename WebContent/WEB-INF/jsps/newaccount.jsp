<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link href="${pageContext.request.contextPath}/static/css/main.css"
	rel="stylesheet" type="text/css">
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
function onLoad(){
	
	$("#password").keyup(checkPasswordsMatch);
	$("#confirmpass").keyup(checkPasswordsMatch);	
	$("#details").submit(canSubmit);		
}

function canSubmit(){
	var password = $("#password").val();
	var confirmpass = $("#confirmpass").val();
	if(password != confirmpass){
		alert("Passwords don't match");
		return false;
	} else {
		return true;
	}
}

function checkPasswordsMatch(){
	var password = $("#password").val();
	var confirmpass = $("#confirmpass").val();
	
	if(password.length > 3 || confirmpass.length > 3) {
	
	if(password==confirmpass){
		$("#matchpass").text("<fmt:message key='MatchedPassword.user.password'/>");
		$("#matchpass").addClass("valid");
		$("#matchpass").removeClass("error");
	} else {
		$("#matchpass").text("<fmt:message key='UnmatchedPassword.user.password'/>");
		$("#matchpass").addClass("error");
		$("#matchpass").removeClass("valid");
	}

	}
	//alert(password + ": " + confirmpass);
}

$(document).ready(onLoad);

</script>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Create a new account</title>
</head>
<body>


	<sf:form method="post"
		action="${pageContext.request.contextPath}/createaccount"
		commandName="user"
		id="details">
		<table class="formtable">
			<tr>
				<td class="label">Username:</td>
				<td><sf:input path="username" name="username" type="text" /><br>
				<div class="error"><sf:errors path="username"></sf:errors></div></td>
			</tr>
			<tr>
				<td class="label">Email:</td>
				<td><sf:input path="email" name="email" type="text" /><br>
				<div class="error"><sf:errors path="email"></sf:errors></div></td>
			</tr>
			<tr>
				<td class="label">Password:</td>
				<td><sf:input path="password" id="password" name="password" type="text" /><br>
				<div class="error"><sf:errors path="password"></sf:errors></div></td>
			</tr>
			<tr>
				<td class="label">Confirm Password:</td>
				<td><input name="confirmpass" id="confirmpass"  type="text"><div id="matchpass"></div></td><br>
			</tr>
			<tr>
				<td>&nbsp;</td>
				<td><input class="control" value="Create account" type="submit" /></td>
			</tr>
		</table>

	</sf:form>

</body>
</html>